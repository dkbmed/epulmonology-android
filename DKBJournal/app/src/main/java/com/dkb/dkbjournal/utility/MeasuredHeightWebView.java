package com.dkb.dkbjournal.utility;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

public class MeasuredHeightWebView extends WebView {
    public MeasuredHeightWebView(Context context) {
        super(context);
    }

    public MeasuredHeightWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MeasuredHeightWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public int getContentHeight() {
        return super.computeVerticalScrollRange(); //working after load of page
    }

}