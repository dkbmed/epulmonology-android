package com.dkb.dkbjournal;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bigkoo.svprogresshud.SVProgressHUD;
import com.lzy.okhttputils.callback.FileCallback;
import com.lzy.okhttputils.request.BaseRequest;

import java.io.File;

import im.delight.android.webview.AdvancedWebView;
import okhttp3.Call;
import okhttp3.Response;

public class WebViewActivity extends AppCompatActivity implements AdvancedWebView.Listener {

    private AdvancedWebView mWebView;
    String getFrom = "";
    TextView textView_title;
    ProgressDialog progressDialog;
    SVProgressHUD mSVProgressHUD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        getFrom = getIntent().getStringExtra("FROM");
        textView_title = (TextView) findViewById(R.id.textView_title);
        ImageButton btn_back = (ImageButton) findViewById(R.id.imageButton_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mSVProgressHUD = new SVProgressHUD(this);
        mSVProgressHUD.showWithStatus(getString(R.string.please_wait), SVProgressHUD.SVProgressHUDMaskType.Clear);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mSVProgressHUD.dismiss();
            }
        }, 5000);

        progressDialog = new ProgressDialog(WebViewActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        mWebView = (AdvancedWebView) findViewById(R.id.webview);
        mWebView.setListener(WebViewActivity.this, this);
        mWebView.setBackgroundColor(Color.parseColor("#d9f2fe"));

        if (getFrom.equalsIgnoreCase("AboutUs")) {
            textView_title.setText("About Us");
            mWebView.loadUrl("http:/dkbmed.com/epulmonology/app/about.html");
        } else if (getFrom.equalsIgnoreCase("CME")) {
            textView_title.setText("CME");
            mWebView.loadUrl("http:/dkbmed.com/epulmonology/app/cme.html");
        } else {
            textView_title.setText("Faculty");
            mWebView.loadUrl("http:/dkbmed.com/epulmonology/app/faculty.html");
        }

    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {

    }

    @Override
    public void onPageFinished(String url) {

    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {

    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    @Override
    public void onExternalPageRequest(String url) {

    }

    @Override
    public void onBackPressed() {
        if (!mWebView.onBackPressed()) {
            return;
        }
        super.onBackPressed();
    }

    @SuppressLint("NewApi")
    @Override
    protected void onPause() {
        mWebView.onPause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mWebView.onDestroy();
        super.onDestroy();
    }

}
