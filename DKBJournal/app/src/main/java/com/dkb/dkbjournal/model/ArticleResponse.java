package com.dkb.dkbjournal.model;

import java.util.List;

/**
 * Created by cbishn on 11/25/17.
 */

public class ArticleResponse {

    List<ArticleData> articles;
    List<UserProgress> user_progress;

    public List<UserProgress> getUser_progress() {
        return user_progress;
    }

    public void setUser_progress(List<UserProgress> user_progress) {
        this.user_progress = user_progress;
    }

    public List<ArticleData> getArticles() {
        return articles;
    }

    public void setArticles(List<ArticleData> articles) {
        this.articles = articles;
    }
}
