package com.dkb.dkbjournal.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ArticleData implements Parcelable {

    public static final Creator<ArticleData> CREATOR = new Creator<ArticleData>() {
        @Override
        public ArticleData createFromParcel(Parcel in) {
            return new ArticleData(in);
        }

        @Override
        public ArticleData[] newArray(int size) {
            return new ArticleData[size];
        }
    };
    private String id;
    private String title;
    private String description;
    private String article_path;
    private String is_published;
    private String thumb;
    private String created_at;
    private String article_category;
    private int progress;

    protected ArticleData(Parcel in) {
        id = in.readString();
        title = in.readString();
        description = in.readString();
        article_path = in.readString();
        is_published = in.readString();
        thumb = in.readString();
        created_at = in.readString();
        article_category = in.readString();
        progress = in.readInt();
    }

    public ArticleData() {

    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getArticle_path() {
        return article_path;
    }

    public void setArticle_path(String article_path) {
        this.article_path = article_path;
    }

    public String getIs_published() {
        return is_published;
    }

    public void setIs_published(String is_published) {
        this.is_published = is_published;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getArticle_category() {
        return article_category;
    }

    public void setArticle_category(String article_category) {
        this.article_category = article_category;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeString(article_path);
        parcel.writeString(is_published);
        parcel.writeString(thumb);
        parcel.writeString(created_at);
        parcel.writeString(article_category);
        parcel.writeInt(progress);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArticleData that = (ArticleData) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
