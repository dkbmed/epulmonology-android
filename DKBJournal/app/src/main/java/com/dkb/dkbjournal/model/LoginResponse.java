package com.dkb.dkbjournal.model;

/**
 * Created by cbishn on 11/18/17.
 */

public class LoginResponse {
    private String userId;
    private String status;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
