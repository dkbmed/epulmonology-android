package com.dkb.dkbjournal;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.URLUtil;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bigkoo.svprogresshud.SVProgressHUD;
import com.dkb.dkbjournal.model.ArticleData;
import com.dkb.dkbjournal.model.ArticleResponse;
import com.dkb.dkbjournal.network.DkbRetrofitBuilder;
import com.dkb.dkbjournal.utility.DBHelper;
import com.dkb.dkbjournal.utility.Global;
import com.lzy.okhttputils.OkHttpUtils;
import com.lzy.okhttputils.callback.FileCallback;
import com.lzy.okhttputils.request.BaseRequest;

import java.io.File;
import java.util.List;

import im.delight.android.webview.AdvancedWebView;
import okhttp3.Call;
import okhttp3.Response;
import retrofit2.Callback;

public class DetailHtmlViewActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String ARG_ARTICLE_DATA = "ARTICLE_DATA";
    private final String TAG = "DetailHtmlViewActivity";
    //private MeasuredHeightWebView webView;
    private AdvancedWebView webView;
    private ImageButton btn_backward, btn_forward, btn_reload, btn_stop;
    private ArticleData articleData;
    private SVProgressHUD mSVProgressHUD;

    public static Intent getIntent(Context context, ArticleData articleData) {
        Intent intent = new Intent(context, DetailHtmlViewActivity.class);
        intent.putExtra(ARG_ARTICLE_DATA, articleData);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_html_view);
        articleData = getIntent().getParcelableExtra(ARG_ARTICLE_DATA);
        mSVProgressHUD = new SVProgressHUD(this);

        ImageButton btn_back = (ImageButton) findViewById(R.id.imageButton_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        TextView txt_title = (TextView) findViewById(R.id.textView_title);
        txt_title.setText(articleData.getTitle());

        btn_backward = (ImageButton) findViewById(R.id.imageButton_backward);
        btn_stop = (ImageButton) findViewById(R.id.imageButton_stop);
        btn_reload = (ImageButton) findViewById(R.id.imageButton_reload);
        btn_forward = (ImageButton) findViewById(R.id.imageButton_forward);

        btn_backward.setEnabled(false);
        btn_forward.setEnabled(false);
        btn_backward.setImageDrawable(getResources().getDrawable(R.mipmap.ic_back_disable));
        btn_forward.setImageDrawable(getResources().getDrawable(R.mipmap.ic_forward_disable));
        btn_backward.setOnClickListener(this);
        btn_forward.setOnClickListener(this);
        btn_reload.setOnClickListener(this);
        btn_stop.setOnClickListener(this);
        webView = (AdvancedWebView) findViewById(R.id.webview);

        String[] items = articleData.getArticle_path().split("\\/");
        String htmlName = articleData.getId() + "_" + items[items.length - 1];
        setupWebView(htmlName);
    }

    private void setupWebView(String htmlName) {
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        settings.setSupportZoom(true);
        webView.setVerticalScrollBarEnabled(true);
        mSVProgressHUD.showWithStatus(getString(R.string.please_wait), SVProgressHUD.SVProgressHUDMaskType.Clear);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mSVProgressHUD.dismiss();
            }
        }, 5000);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                super.shouldOverrideUrlLoading(webView, url);
                Log.i(TAG, "Processing webview url click...");
                Log.e("url ", " : " + url);
                if (URLUtil.isNetworkUrl(url) && url.contains("http")) {
                    view.loadUrl(url);
                    return false;
                }
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Log.i(TAG, "Finished loading URL: " + url);
                int progress = getProgress();
                webView.setScrollY(progress);
                onSetBackForwardState();
                mSVProgressHUD.dismiss();
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Log.e(TAG, "Error: " + description);
                onSetBackForwardState();
                mSVProgressHUD.dismiss();
            }
        });
        webView.setWebChromeClient(new WebChromeClient());

        if (Global.is_online.equals("online")) {
            webView.clearView();
            webView.loadUrl(articleData.getArticle_path());
            File file = new File(Environment.getExternalStorageDirectory() + "/DBKJournal/", htmlName);
            if (!file.exists()) {
                OkHttpUtils.get(articleData.getArticle_path())
                        .tag(this)
                        .execute(new DownloadFileCallBack(Environment.getExternalStorageDirectory() + "/DBKJournal", htmlName));
            }
        } else {
            File file = new File(Environment.getExternalStorageDirectory() + "/DBKJournal/", htmlName);
            if (file.exists()) {
                webView.loadUrl("file:///" + file.getAbsolutePath());
            }
        }
    }

    private int getProgress() {
        int index = DBHelper.getIndex(articleData.getId());
        if (index != -1) {
            return index;
        } else if (articleData.getProgress() > 0) {
            float positionInWV = webView.getContentHeight() * articleData.getProgress();
            index = (int) ((double) (positionInWV) / (double) 100);
            if (index > webView.getBottom()) {
                return index - webView.getBottom();
            } else {
                return index;
            }
        } else {
            return 0;
        }
    }

    private void onSetBackForwardState() {
        if (webView.canGoBack()) {
            btn_backward.setEnabled(true);
            btn_backward.setImageDrawable(getResources().getDrawable(R.mipmap.ic_back));
        } else {
            btn_backward.setEnabled(false);
            btn_backward.setImageDrawable(getResources().getDrawable(R.mipmap.ic_back_disable));
        }

        if (webView.canGoForward()) {
            btn_forward.setEnabled(true);
            btn_forward.setImageDrawable(getResources().getDrawable(R.mipmap.ic_forward));
        } else {
            btn_forward.setEnabled(false);
            btn_forward.setImageDrawable(getResources().getDrawable(R.mipmap.ic_forward_disable));
        }
    }


    @Override
    public void onBackPressed() {
        int progress = (int) calculateProgression();
        DBHelper.saveIndex(articleData.getId(), webView.getScrollY());
        saveProgress(progress);
    }

    private float calculateProgression() {
        webView.computeScroll();
        float contentHeight = webView.getContentHeight();
        float currentScrollPosition = webView.getScrollY();
        float percentWebview = (currentScrollPosition + webView.getBottom()) / contentHeight;
        return percentWebview * 100;
    }

    private void saveProgress(final int progress) {
        new DkbRetrofitBuilder().getService().saveArticleProgress(articleData.getId(), DBHelper.getUserId(), progress).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(retrofit2.Call<Void> call, retrofit2.Response<Void> response) {
                Log.d("Successful", "success");
                ArticleResponse articleResponse = DBHelper.getArticleData();
                List<ArticleData> articles = articleResponse.getArticles();
                articleData.setProgress(progress);
                int i = articles.indexOf(articleData);
                articles.set(i, articleData);
                articleResponse.setArticles(articles);
                DBHelper.saveArticleDataWithoutProgress(articleResponse);
                finish();
            }

            @Override
            public void onFailure(retrofit2.Call<Void> call, Throwable t) {
                Log.d("Failure", "failure");
                finish();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageButton_backward:
                if (webView.canGoBack()) {
                    webView.goBack();
                }
                onSetBackForwardState();
                break;
            case R.id.imageButton_stop:
                webView.stopLoading();
                onSetBackForwardState();
                break;
            case R.id.imageButton_reload:
                webView.reload();
                onSetBackForwardState();
                break;
            case R.id.imageButton_forward:
                if (webView.canGoForward()) {
                    webView.goForward();
                }
                onSetBackForwardState();
                break;
        }

    }

    private class DownloadFileCallBack extends FileCallback {

        public DownloadFileCallBack(String destFileDir, String destFileName) {
            super(destFileDir, destFileName);
        }

        @Override
        public void onBefore(BaseRequest request) {
        }

        @Override
        public void onSuccess(File file, Call call, Response response) {

        }

        /*@Override
        public void onResponse(boolean isFromCache, File file, Request request, Response response) {
        }*/

        @Override
        public void downloadProgress(long currentSize, long totalSize, float progress, long networkSpeed) {
            System.out.println("downloadProgress -- " + totalSize + "  " + currentSize + "  " + progress + "  " + networkSpeed);
        }

        /*@Override
        public void onError(boolean isFromCache, Call call, @Nullable Response response, @Nullable Exception e) {
            super.onError(isFromCache, call, response, e);
        }*/
    }


}
