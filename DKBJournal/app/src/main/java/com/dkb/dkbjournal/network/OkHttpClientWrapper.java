package com.dkb.dkbjournal.network;

import com.facebook.stetho.okhttp3.StethoInterceptor;

import okhttp3.OkHttpClient;

/**
 * Build-Config-Specific wrapper utility that modifies a given {@link OkHttpClient} instance
 * for debug builds.
 * <p>
 * Created by mware on 2/7/17.
 */
public final class OkHttpClientWrapper {

    public OkHttpClientWrapper() throws Exception {
        throw new Exception("No instances.");
    }

    public static void setupNetworkInterceptors(OkHttpClient.Builder clientBuilder) {
        // Intercept network calls for Stetho during debug builds.
        clientBuilder.addNetworkInterceptor(new StethoInterceptor());
    }
}
