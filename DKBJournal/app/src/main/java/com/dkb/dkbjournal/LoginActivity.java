package com.dkb.dkbjournal;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bigkoo.alertview.AlertView;
import com.bigkoo.svprogresshud.SVProgressHUD;
import com.dkb.dkbjournal.model.LoginResponse;
import com.dkb.dkbjournal.network.DkbRetrofitBuilder;
import com.dkb.dkbjournal.utility.DBHelper;
import com.dkb.dkbjournal.utility.Global;
import com.kyleduo.switchbutton.SwitchButton;

import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    public SVProgressHUD mSVProgressHUD;
    EditText edit_email;
    RelativeLayout relativeLayout_alert;
    TextView text_alert;
    SwitchButton switch_remember;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edit_email = (EditText) findViewById(R.id.editText_email);

        text_alert = (TextView) findViewById(R.id.textView_alert_content);
        relativeLayout_alert = (RelativeLayout) findViewById(R.id.relativeLayout_alert);
        relativeLayout_alert.setVisibility(View.INVISIBLE);

        Button btn_alert_ok = (Button) findViewById(R.id.button_alert_ok);
        btn_alert_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                relativeLayout_alert.setVisibility(View.INVISIBLE);
            }
        });

        switch_remember = (SwitchButton) findViewById(R.id.switch_remember);
        switch_remember.setEnabled(true);

        Button btn_login = (Button) findViewById(R.id.button_login);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edit_email.getText().toString().equals("")) {
                    new AlertView(getString(R.string.alert_title),
                            getString(R.string.alert_enter_email), null,
                            new String[]{getString(R.string.alert_ok)}, null, LoginActivity.this,
                            AlertView.Style.Alert, null).show();
                    return;
                } else if (!Global.isValidEmail(edit_email.getText().toString())) {
                    new AlertView(getString(R.string.alert_title),
                            getString(R.string.alert_enter_email_correct), null,
                            new String[]{getString(R.string.alert_ok)}, null, LoginActivity.this,
                            AlertView.Style.Alert, null).show();
                    return;
                } else {

                    mSVProgressHUD = new SVProgressHUD(LoginActivity.this);
                    mSVProgressHUD.showWithStatus(getString(R.string.please_wait), SVProgressHUD.SVProgressHUDMaskType.Clear);
                    new DkbRetrofitBuilder().getService().login(edit_email.getText().toString()).enqueue(new Callback<LoginResponse>() {
                        @Override
                        public void onResponse(retrofit2.Call<LoginResponse> call, Response<LoginResponse> response) {
                            mSVProgressHUD.dismiss();
                            LoginResponse loginResponse = response.body();
                            if (loginResponse == null || loginResponse.getStatus().equals("0")) {
                                text_alert.setText(getString(R.string.alert_invalid_email));
                                relativeLayout_alert.setVisibility(View.VISIBLE);
                            } else if (loginResponse.getStatus().equals("1")) {
                                if (switch_remember.isChecked()) {
                                    SharedPreferences s = getSharedPreferences("user", 0);
                                    SharedPreferences.Editor edit = s.edit();
                                    edit.putString("email", edit_email.getText().toString());
                                    edit.putString("status", "1");
                                    edit.apply();
                                    DBHelper.saveUserId(loginResponse.getUserId());
                                }

                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                            } else {
                                text_alert.setText(getString(R.string.alert_login_failed));
                                relativeLayout_alert.setVisibility(View.VISIBLE);
                            }
                        }

                        @Override
                        public void onFailure(retrofit2.Call<LoginResponse> call, Throwable t) {
                            mSVProgressHUD.dismiss();
                            text_alert.setText(getString(R.string.alert_login_failed));
                            relativeLayout_alert.setVisibility(View.VISIBLE);
                        }
                    });
                }
            }
        });

        Button btn_signup = (Button) findViewById(R.id.button_signup);
        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

}
