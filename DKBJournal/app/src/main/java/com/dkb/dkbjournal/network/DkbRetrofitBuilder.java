package com.dkb.dkbjournal.network;


import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DkbRetrofitBuilder {

    private static Retrofit sRetrofit;
    private DkbService mDkbService;

    public DkbRetrofitBuilder() {
        String baseUrl = "http://epulm.eliteraturereview.org/public/";

        //Logging Interceptor
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        // LogInterceptor
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        httpClient.readTimeout(120, TimeUnit.SECONDS);
        httpClient.connectTimeout(120, TimeUnit.SECONDS);

        if (sRetrofit == null) {
            sRetrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(httpClient.build())
                    .addConverterFactory(GsonConverterFactory.create(new Gson()))
                    .build();
        }
    }

    public DkbService getService() {
        if (mDkbService == null) {
            mDkbService = sRetrofit.create(DkbService.class);
        }
        return mDkbService;
    }

}

