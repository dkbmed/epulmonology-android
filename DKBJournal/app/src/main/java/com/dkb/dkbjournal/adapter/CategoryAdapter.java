package com.dkb.dkbjournal.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dkb.dkbjournal.R;
import com.dkb.dkbjournal.model.Category;
import com.dkb.dkbjournal.utility.DBHelper;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by cbishn on 11/25/17.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {
    private Context context;
    private List<Category> categoryList;
    private Set<String> selectedCategories = new HashSet<>();

    public CategoryAdapter(Context context) {
        this.context = context;
        selectedCategories = DBHelper.getSelectedCategories();
    }

    public Set<String> getSelectedCategories() {
        return selectedCategories;
    }

    public void setData(List<Category> categoryList) {
        this.categoryList = categoryList;
        notifyDataSetChanged();
    }

    public void clearSelection() {
        selectedCategories = new HashSet<>();
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.row_filter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Category category = categoryList.get(position);
        holder.categoryName.setText(category.getCategory());
        holder.parentCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.checkBox.setChecked(!holder.checkBox.isChecked());
            }
        });
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    selectedCategories.add(category.getId());
                } else {
                    selectedCategories.remove(category.getId());
                }
            }
        });
        holder.checkBox.setChecked(selectedCategories != null && selectedCategories.contains(category.getId()));
    }

    @Override
    public int getItemCount() {
        return categoryList != null ? categoryList.size() : 0;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.checkbox) CheckBox checkBox;
        @BindView(R.id.category_name) TextView categoryName;
        @BindView(R.id.parent_category_name) RelativeLayout parentCategory;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
