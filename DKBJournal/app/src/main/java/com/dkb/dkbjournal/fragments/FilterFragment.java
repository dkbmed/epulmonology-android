package com.dkb.dkbjournal.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.dkb.dkbjournal.MainActivity;
import com.dkb.dkbjournal.R;
import com.dkb.dkbjournal.adapter.CategoryAdapter;
import com.dkb.dkbjournal.model.Category;
import com.dkb.dkbjournal.network.DkbRetrofitBuilder;
import com.dkb.dkbjournal.utility.DBHelper;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by cbishn on 11/25/17.
 */

public class FilterFragment extends DialogFragment {
    @BindView(R.id.cateogry_recycler_view) RecyclerView recyclerView;
    @BindView(R.id.loader) ProgressBar progressBar;
    private CategoryAdapter categoryAdapter;
    private Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_filter, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        categoryAdapter = new CategoryAdapter(context);
        recyclerView.setAdapter(categoryAdapter);
        List<Category> categories = DBHelper.getCategories();
        if (categories != null) {
            categoryAdapter.setData(categories);
        } else {
            loadCategories();
        }
    }

    private void loadCategories() {
        progressBar.setVisibility(View.VISIBLE);
        new DkbRetrofitBuilder().getService().getCategories().enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                categoryAdapter.setData(response.body());
                DBHelper.saveCategories(response.body());
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                Timber.e(t.getMessage());
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @OnClick(R.id.clear)
    public void clear() {
        categoryAdapter.clearSelection();
        DBHelper.saveSelectedCategories(new HashSet<String>());
        ((MainActivity) context).clearFilters();
        dismiss();
    }

    @OnClick(R.id.cancel)
    public void cancel() {
        dismiss();
    }

    @OnClick(R.id.apply)
    public void apply() {
        Set<String> selectedCategories = categoryAdapter.getSelectedCategories();
        DBHelper.saveSelectedCategories(selectedCategories);
        if (selectedCategories.isEmpty()) {
            ((MainActivity) context).clearFilters();
        } else {
            ((MainActivity) context).applyFilters(selectedCategories);
        }
        dismiss();
    }
}
