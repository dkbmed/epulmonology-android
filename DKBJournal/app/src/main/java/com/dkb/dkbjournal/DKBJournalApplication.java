package com.dkb.dkbjournal;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.dkb.dkbjournal.network.OkHttpClientWrapper;
import com.facebook.stetho.Stetho;
import com.facebook.stetho.timber.StethoTree;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.lzy.okhttputils.cookie.store.PersistentCookieStore;
import com.lzy.okhttputils.model.HttpParams;
import com.onesignal.OneSignal;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.https.HttpsUtils;
import com.zhy.http.okhttp.log.LoggerInterceptor;

import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import io.paperdb.Paper;
import okhttp3.OkHttpClient;
import timber.log.Timber;

public class DKBJournalApplication extends Application {

    public static GoogleAnalytics sAnalytics;
    public static Tracker sTracker;

    @Override
    public void onCreate() {
        super.onCreate();
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();

        HttpsUtils.SSLParams sslParams = HttpsUtils.getSslSocketFactory(null, null, null);

        OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(30000L, TimeUnit.MILLISECONDS)
                .readTimeout(30000L, TimeUnit.MILLISECONDS)
                .addInterceptor(new LoggerInterceptor("TAG"))
                .hostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname, SSLSession session) {
                        return true;
                    }
                })
                .sslSocketFactory(sslParams.sSLSocketFactory, sslParams.trustManager);
        OkHttpClientWrapper.setupNetworkInterceptors(okHttpClient);
        OkHttpUtils.initClient(okHttpClient.build());


        HttpParams params = new HttpParams();

        com.lzy.okhttputils.OkHttpUtils.init(this);
        com.lzy.okhttputils.OkHttpUtils.getInstance()
                .debug("OkHttpUtils")
                .setConnectTimeout(com.lzy.okhttputils.OkHttpUtils.DEFAULT_MILLISECONDS)
                .setReadTimeOut(com.lzy.okhttputils.OkHttpUtils.DEFAULT_MILLISECONDS)
                .setWriteTimeOut(com.lzy.okhttputils.OkHttpUtils.DEFAULT_MILLISECONDS)
                .setCookieStore(new PersistentCookieStore())
                .addCommonParams(params);

        Paper.init(this);
        initializeStetho();

        // Send Timber log calls to Stetho
        Timber.plant(new StethoTree());
        Timber.plant(new Timber.DebugTree());

        sAnalytics = GoogleAnalytics.getInstance(this);
    }

    private void initializeStetho() {
        Stetho.initializeWithDefaults(this);
    }

    synchronized public Tracker getDefaultTracker() {
        if (sTracker == null) {
            sTracker = sAnalytics.newTracker(R.xml.global_tracker);
        }
        return sTracker;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
