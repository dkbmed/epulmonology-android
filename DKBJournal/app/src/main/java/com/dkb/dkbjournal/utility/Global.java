package com.dkb.dkbjournal.utility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import org.json.JSONException;
import org.json.JSONObject;

public class Global {

    public static final String SERVER_LOGIN = "http://epulm.eliteraturereview.org/public/api/login";

    /*
     * API Info.
     */
    public static final String SERVER_SIGNUP = "http://epulm.eliteraturereview.org/public/api/register";
    public static final String SERVER_PROFESSION = "http://epulm.eliteraturereview.org/public/api/professions";
    public static final String SERVER_SPECIALTY = "http://epulm.eliteraturereview.org/public/api/specialties";
    public static final String KEY_ID = "id";
    public static final String KEY_TITLE = "title";
    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_FIST_NAME = "first_name";
    public static final String KEY_LAST_NAME = "last_name";
    public static final String KEY_PROFESSION_ID = "profession_id";
    public static final String KEY_SPECIALITIES_ID = "specialities_id";
    public static String is_online = "online";

    public static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static String onCheckNull(JSONObject e, String Key) {
        try {
            if ((e.getString(Key)).equals(null)) {
                return "";
            } else if ((e.getString(Key)).equals("null")) {
                return "";
            } else {
                return e.getString(Key);
            }
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            return "";
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
