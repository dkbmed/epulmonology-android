package com.dkb.dkbjournal.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.dkb.dkbjournal.MainActivity;
import com.dkb.dkbjournal.R;
import com.dkb.dkbjournal.model.Sort;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

/**
 * Created by cbishn on 11/25/17.
 */

public class SortFragment extends DialogFragment {
    @BindView(R.id.checkbox_released_7_days) CheckBox articleReleasedLast7Days;
    @BindView(R.id.checkbox_article_not_completed) CheckBox articleNotCompleted;
    @BindView(R.id.checkbox_articles_never_openend) CheckBox articleNeverOpened;
    @BindView(R.id.checkbox_articles_completed) CheckBox articlesCompleted;
    private MainActivity mainActivity;
    private Sort selectedSort;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainActivity = (MainActivity) context;
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sort, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @OnClick(R.id.parent_released_7_days)
    public void onParentCheckArticleReleased() {
        articleReleasedLast7Days.setChecked(!articleReleasedLast7Days.isChecked());
    }

    @OnCheckedChanged(R.id.checkbox_released_7_days)
    public void onCheckArticleReleased(CheckBox checkBox) {
        articleNotCompleted.setOnCheckedChangeListener(null);
        articleNotCompleted.setChecked(false);
        articleNeverOpened.setOnCheckedChangeListener(null);
        articleNeverOpened.setChecked(false);
        articleNotCompleted.setOnCheckedChangeListener(null);
        articlesCompleted.setChecked(false);
        ButterKnife.bind(this, getView());
        if (checkBox.isChecked()) {
            selectedSort = Sort.OPEN_DATE;
        } else {
            selectedSort = Sort.DEFAULT;
        }
    }

    @OnClick(R.id.parent_article_not_completed)
    public void onParentCheckArticleNotCompleted() {
        articleNotCompleted.setChecked(!articleNotCompleted.isChecked());
    }

    @OnCheckedChanged(R.id.checkbox_article_not_completed)
    public void onCheckArticleNotCompleted(CheckBox checkBox) {
        articleReleasedLast7Days.setOnCheckedChangeListener(null);
        articleReleasedLast7Days.setChecked(false);
        articleNeverOpened.setOnCheckedChangeListener(null);
        articleNeverOpened.setChecked(false);
        articlesCompleted.setOnCheckedChangeListener(null);
        articlesCompleted.setChecked(false);
        ButterKnife.bind(this, getView());
        if (checkBox.isChecked()) {
            selectedSort = Sort.NOT_COMPLETED;
        } else {
            selectedSort = Sort.DEFAULT;
        }
    }

    @OnClick(R.id.parent_articles_never_openend)
    public void onParentCheckArticleNeverOpened() {
        articleNeverOpened.setChecked(!articleNeverOpened.isChecked());
    }

    @OnCheckedChanged(R.id.checkbox_articles_never_openend)
    public void onCheckArticleNeverOpened(CheckBox checkBox) {
        articleReleasedLast7Days.setOnCheckedChangeListener(null);
        articleReleasedLast7Days.setChecked(false);
        articleNotCompleted.setOnCheckedChangeListener(null);
        articleNotCompleted.setChecked(false);
        articlesCompleted.setOnCheckedChangeListener(null);
        articlesCompleted.setChecked(false);
        ButterKnife.bind(this, getView());
        if (checkBox.isChecked()) {
            selectedSort = Sort.NEVER_OPENED;
        } else {
            selectedSort = Sort.DEFAULT;
        }
    }

    @OnClick(R.id.parent_articles_completed)
    public void onParentCheckArticleCompleted() {
        articlesCompleted.setChecked(!articlesCompleted.isChecked());
    }

    @OnCheckedChanged(R.id.checkbox_articles_completed)
    public void onCheckArticleCompleted(CheckBox checkBox) {
        articleReleasedLast7Days.setOnCheckedChangeListener(null);
        articleReleasedLast7Days.setChecked(false);
        articleNotCompleted.setOnCheckedChangeListener(null);
        articleNotCompleted.setChecked(false);
        articleNeverOpened.setOnCheckedChangeListener(null);
        articleNeverOpened.setChecked(false);
        ButterKnife.bind(this, getView());
        if (checkBox.isChecked()) {
            selectedSort = Sort.COMPLETED;
        } else {
            selectedSort = Sort.DEFAULT;
        }
    }

    @OnClick(R.id.clear)
    public void clear() {
        articleReleasedLast7Days.setOnCheckedChangeListener(null);
        articleReleasedLast7Days.setChecked(false);
        articleNotCompleted.setOnCheckedChangeListener(null);
        articleNotCompleted.setChecked(false);
        articleNeverOpened.setOnCheckedChangeListener(null);
        articleNeverOpened.setChecked(false);
        articlesCompleted.setOnCheckedChangeListener(null);
        articlesCompleted.setChecked(false);
        ButterKnife.bind(this, getView());
        dismiss();
        mainActivity.applySorting(Sort.DEFAULT);
    }

    @OnClick(R.id.cancel)
    public void cancel() {
        dismiss();
    }

    @OnClick(R.id.apply)
    public void apply() {
        dismiss();
        mainActivity.applySorting(selectedSort);
    }
}
