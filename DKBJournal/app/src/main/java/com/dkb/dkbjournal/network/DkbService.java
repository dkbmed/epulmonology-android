package com.dkb.dkbjournal.network;

import com.dkb.dkbjournal.model.ArticleData;
import com.dkb.dkbjournal.model.ArticleResponse;
import com.dkb.dkbjournal.model.Category;
import com.dkb.dkbjournal.model.LoginResponse;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface DkbService {

    @GET("/public/api/article/user/id/{userId}")
    Call<ArticleResponse> getArticles(@Path("userId") String userId, @Query("is_published") String articleId);

    @GET("/public/api/article/user/id/{userId}")
    Call<ArticleResponse> getArticles(@Path("userId") String userId);

    @GET("/public/api/article/progress/set-progress-info")
    Call<Void> saveArticleProgress(@Query("article_id") String articleId, @Query("user_id") String userId, @Query("progress_info") int progressInfo);

    @GET("/public/api/category/all")
    Call<List<Category>> getCategories();

    @GET("/public/api/article/search/keywords")
    Call<List<ArticleData>> search(@Query("s") String searchText);

    @GET
    Call<ResponseBody> downloadFile(@Url String fileUrl);

    @GET("/public/api/login")
    Call<LoginResponse> login(@Query("email") String email);
}
