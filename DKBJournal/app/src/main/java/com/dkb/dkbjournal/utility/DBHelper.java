package com.dkb.dkbjournal.utility;

import android.text.TextUtils;

import com.dkb.dkbjournal.model.ArticleData;
import com.dkb.dkbjournal.model.ArticleResponse;
import com.dkb.dkbjournal.model.Category;
import com.dkb.dkbjournal.model.UserProgress;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.paperdb.Paper;

/**
 * Created by cbishn on 11/18/17.
 */

public class DBHelper {
    private static final String USER_ID = "userId";
    private static final String ARTICLE_ID = "article_data";
    private static final String CATEGORIES = "categories";
    private static final String SELECTED_CATEGORIES = "selected_categories";

    public static void saveIndex(String articleId, int index) {
        if (articleId != null) {
            Paper.book().write(articleId, index);
        }
    }

    public static int getIndex(String articleId) {
        if (!TextUtils.isEmpty(articleId) && Paper.book().exist(articleId)) {
            return Paper.book().read(articleId);
        }
        return -1;
    }

    public static void saveUserId(String userId) {
        if (userId != null) {
            Paper.book().write(USER_ID, userId);
        }
    }

    public static String getUserId() {
        return Paper.book().read(USER_ID);
    }

    public static void saveArticleData(ArticleResponse articleResponse) {
        if (articleResponse != null) {
            List<ArticleData> articleDataList = articleResponse.getArticles();
            for (ArticleData articleData : articleDataList) {
                float progress = getProgress(articleData, articleResponse);
                articleData.setProgress((int) progress);
            }
            articleResponse.setArticles(articleDataList);
            Paper.book().write(ARTICLE_ID, articleResponse);
        }
    }

    public static void saveArticleDataWithoutProgress(ArticleResponse articleResponse) {
        if (articleResponse != null) {
            Paper.book().write(ARTICLE_ID, articleResponse);
        }
    }

    private static float getProgress(ArticleData articleData, ArticleResponse articleResponse) {
        for (UserProgress userProgress :
                articleResponse.getUser_progress()) {
            if (userProgress.getArticle_id().equals(articleData.getId())) {
                return userProgress.getProgress_info();
            }

        }
        return 0;
    }

    public static ArticleResponse getArticleData() {
        return Paper.book().read(ARTICLE_ID);
    }

    public static void saveSelectedCategories(Set<String> selectedCategories) {
        if (selectedCategories != null) {
            Paper.book().write(SELECTED_CATEGORIES, selectedCategories);
        }
    }

    public static Set<String> getSelectedCategories() {
        Set<String> selectedCategories = Paper.book().read(SELECTED_CATEGORIES);
        return selectedCategories != null ? selectedCategories : new HashSet<String>();
    }

    public static void saveCategories(List<Category> categoryList) {
        if (categoryList != null) {
            Paper.book().write(CATEGORIES, categoryList);
        }
    }

    public static List<Category> getCategories() {
        return Paper.book().read(CATEGORIES);
    }

    public static void clearDB() {
        Paper.book().destroy();
    }
}
