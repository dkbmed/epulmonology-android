package com.dkb.dkbjournal;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.alertview.AlertView;
import com.bigkoo.svprogresshud.SVProgressHUD;
import com.dkb.dkbjournal.adapter.ArticleDataAdapter;
import com.dkb.dkbjournal.fragments.FilterFragment;
import com.dkb.dkbjournal.fragments.SortFragment;
import com.dkb.dkbjournal.model.ArticleData;
import com.dkb.dkbjournal.model.ArticleResponse;
import com.dkb.dkbjournal.model.Sort;
import com.dkb.dkbjournal.network.DkbRetrofitBuilder;
import com.dkb.dkbjournal.utility.ArticleNameComparator;
import com.dkb.dkbjournal.utility.DBHelper;
import com.dkb.dkbjournal.utility.Global;
import com.google.android.gms.analytics.HitBuilders;
import com.qbw.customview.RefreshLoadMoreLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener, RefreshLoadMoreLayout.CallBack {

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @BindView(R.id.relativeLayout_alert)
    RelativeLayout relativeLayout_alert;
    @BindView(R.id.refreshloadmore)
    RefreshLoadMoreLayout mRefreshloadmore;
    @BindView(R.id.listview)
    ListView mListview;
    @BindView(R.id.search_box)
    EditText mSearchBox;
    @BindView(R.id.clear)
    TextView clearSearch;

    private SVProgressHUD mSVProgressHUD;
    private FragmentDrawer drawerFragment;
    private SortFragment sortFragment;
    private FilterFragment filterFragment;

    private ArticleDataAdapter mAdapter;
    private Set<String> selectedCategories;
    private long exitTime = 0;
    private Sort selectedSort;
    private List<ArticleData> activeArticleData;
    String getEmail = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        DKBJournalApplication application = (DKBJournalApplication) getApplication();
        DKBJournalApplication.sTracker = application.getDefaultTracker();
        DKBJournalApplication.sTracker.send(new HitBuilders.EventBuilder("ui", "open")
                .setLabel("MainActivity")
                .build());
        SharedPreferences s = getSharedPreferences("user", 0);
        getEmail = s.getString("email", "");

        setUpDrawer();
        mSVProgressHUD = new SVProgressHUD(MainActivity.this);
        mRefreshloadmore.init(new RefreshLoadMoreLayout.Config(this));
        mRefreshloadmore.setCanLoadMore(false);

        relativeLayout_alert = (RelativeLayout) findViewById(R.id.relativeLayout_alert);
        relativeLayout_alert.setVisibility(View.INVISIBLE);

        sortFragment = new SortFragment();
        filterFragment = new FilterFragment();

        mAdapter = new ArticleDataAdapter(this);
        mListview.setAdapter(mAdapter);

        if (!Global.isNetworkAvailable(MainActivity.this)) {
            onLoadArticleDataFromLocal();
            new AlertView(getString(R.string.alert_title),
                    getString(R.string.alert_no_internet), null,
                    new String[]{getString(R.string.alert_ok)}, null, MainActivity.this,
                    AlertView.Style.Alert, null).show();
        } else {
            mSVProgressHUD.showWithStatus(getString(R.string.please_wait), SVProgressHUD.SVProgressHUDMaskType.Clear);
            initLoadArticleData();
        }

        mSearchBox.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch();
                    return true;
                }
                return false;
            }
        });

    }

    private void performSearch() {
        String searchText = mSearchBox.getText().toString().trim();
        if (TextUtils.isEmpty(searchText)) {
            return;
        }
        mSVProgressHUD.showWithStatus(getString(R.string.please_wait), SVProgressHUD.SVProgressHUDMaskType.Clear);
        new DkbRetrofitBuilder().getService().search(searchText).enqueue(new Callback<List<ArticleData>>() {
            @Override
            public void onResponse(Call<List<ArticleData>> call, Response<List<ArticleData>> response) {
                mAdapter.removeAll();
                activeArticleData = response.body();
                applyFilters(selectedCategories);
                mSVProgressHUD.dismiss();
            }

            @Override
            public void onFailure(Call<List<ArticleData>> call, Throwable t) {
                mAdapter.removeAll();
                activeArticleData = new ArrayList<>();
                applyFilters(selectedCategories);
                mSVProgressHUD.dismiss();
            }
        });
    }

    private void setUpDrawer() {
        drawerFragment = (FragmentDrawer) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, mDrawerLayout, null);
        drawerFragment.setDrawerListener(this);
    }

    public void initLoadArticleData() {
        if (getEmail.equalsIgnoreCase("info@dkbmed.com")) {
            new DkbRetrofitBuilder().getService().getArticles(DBHelper.getUserId()).enqueue(new Callback<ArticleResponse>() {
                @Override
                public void onResponse(retrofit2.Call<ArticleResponse> call, Response<ArticleResponse> response) {
                    Log.d("Respond", response.toString());
                    mSVProgressHUD.dismiss();
                    if (response.body() == null) {
                        relativeLayout_alert.setVisibility(View.VISIBLE);
                        onLoadArticleDataFromLocal();
                    } else {
                        relativeLayout_alert.setVisibility(View.INVISIBLE);
                        saveArticleData(response.body());
                    }
                }

                @Override
                public void onFailure(retrofit2.Call<ArticleResponse> call, Throwable t) {
                    Log.d("Respond Error", t.toString());
                    mSVProgressHUD.dismiss();
                    relativeLayout_alert.setVisibility(View.VISIBLE);
                    onLoadArticleDataFromLocal();
                }
            });
        } else {
            new DkbRetrofitBuilder().getService().getArticles(DBHelper.getUserId(), "1").enqueue(new Callback<ArticleResponse>() {
                @Override
                public void onResponse(retrofit2.Call<ArticleResponse> call, Response<ArticleResponse> response) {
                    Log.d("Respond", response.toString());
                    mSVProgressHUD.dismiss();
                    if (response.body() == null) {
                        relativeLayout_alert.setVisibility(View.VISIBLE);
                        onLoadArticleDataFromLocal();
                    } else {
                        relativeLayout_alert.setVisibility(View.INVISIBLE);
                        saveArticleData(response.body());
                    }
                }

                @Override
                public void onFailure(retrofit2.Call<ArticleResponse> call, Throwable t) {
                    Log.d("Respond Error", t.toString());
                    mSVProgressHUD.dismiss();
                    relativeLayout_alert.setVisibility(View.VISIBLE);
                    onLoadArticleDataFromLocal();
                }
            });
        }

    }

    private void saveArticleData(ArticleResponse articleResponse) {
        mAdapter.removeAll();
        List<ArticleData> articles = articleResponse.getArticles();
        Collections.sort(articles, new ArticleNameComparator());
        articleResponse.setArticles(articles);
        DBHelper.saveArticleData(articleResponse);
        activeArticleData = articles;
        mAdapter.setData(activeArticleData);
        mAdapter.notifyDataSetChanged();
    }

    private void onLoadArticleDataFromLocal() {
        ArticleResponse articleData = DBHelper.getArticleData();
        if (articleData != null && articleData.getArticles() != null && !articleData.getArticles().isEmpty()) {
            activeArticleData = articleData.getArticles();
            relativeLayout_alert.setVisibility(View.INVISIBLE);
            List<ArticleData> articles = articleData.getArticles();
            Collections.sort(articles, new ArticleNameComparator());
            articleData.setArticles(articles);
            mAdapter.setData(articleData.getArticles());
        } else {
            relativeLayout_alert.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRefresh() {
        if (getEmail.equalsIgnoreCase("info@dkbmed.com")) {
            new DkbRetrofitBuilder().getService().getArticles(DBHelper.getUserId()).enqueue(new Callback<ArticleResponse>() {
                @Override
                public void onResponse(retrofit2.Call<ArticleResponse> call, Response<ArticleResponse> response) {
                    mRefreshloadmore.stopRefresh();
                    saveArticleData(response.body());
                }

                @Override
                public void onFailure(retrofit2.Call<ArticleResponse> call, Throwable t) {
                    Log.d("Respond Error", t.toString());
                    mRefreshloadmore.stopRefresh();
                }
            });
        } else {
            new DkbRetrofitBuilder().getService().getArticles(DBHelper.getUserId(), "1").enqueue(new Callback<ArticleResponse>() {
                @Override
                public void onResponse(retrofit2.Call<ArticleResponse> call, Response<ArticleResponse> response) {
                    mRefreshloadmore.stopRefresh();
                    saveArticleData(response.body());
                }

                @Override
                public void onFailure(retrofit2.Call<ArticleResponse> call, Throwable t) {
                    Log.d("Respond Error", t.toString());
                    mRefreshloadmore.stopRefresh();
                }
            });
        }

    }

    @Override
    public void onLoadMore() {
        mRefreshloadmore.stopLoadMore();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (DBHelper.getArticleData() != null) {
            List<ArticleData> articles = DBHelper.getArticleData().getArticles();
            if (activeArticleData != null && articles != null) {
                for (ArticleData articleData : articles) {
                    for (ArticleData articleData1 : activeArticleData) {
                        if (articleData1.getId().equals(articleData.getId())) {
                            articleData1.setProgress(articleData.getProgress());
                        }
                    }
                }
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    private void displayView(int position) {
        switch (position) {
            case 0:
                mDrawerLayout.closeDrawer(Gravity.LEFT);
                break;
            case 1:
                Intent intent = new Intent(MainActivity.this, WebViewActivity.class);
                intent.putExtra("FROM", "AboutUs");
                startActivity(intent);
                break;
            case 2:
                Intent intent1 = new Intent(MainActivity.this, WebViewActivity.class);
                intent1.putExtra("FROM", "CME");
                startActivity(intent1);
                break;
            case 3:
                Intent intent2 = new Intent(MainActivity.this, WebViewActivity.class);
                intent2.putExtra("FROM", "Faculty");
                startActivity(intent2);
                break;
            default:
                break;
        }
    }

    public void doExitApp() {
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            Toast.makeText(this, R.string.press_again_exit_app, Toast.LENGTH_SHORT).show();
            exitTime = System.currentTimeMillis();
        } else {
            DBHelper.saveSelectedCategories(new HashSet<String>());
            finish();
            System.exit(0);
        }
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawer(Gravity.LEFT);
        } else {
            doExitApp();
        }
    }

    @OnTextChanged(value = R.id.search_box,
            callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    void inputSearchText(Editable editable) {
        String searchText = mSearchBox.getText().toString().trim();
        clearSearch.setVisibility(TextUtils.isEmpty(searchText) ? View.GONE : View.VISIBLE);
    }

    @OnClick(R.id.clear)
    public void clearSearch() {
        activeArticleData = DBHelper.getArticleData().getArticles();
        mSearchBox.setText("");
        if (selectedSort == null) {
            selectedSort = Sort.DEFAULT;
        }
        applySorting(selectedSort);
    }

    @OnClick(R.id.sort)
    public void onClickSort() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_in_bottom);
        sortFragment.show(transaction, null);
    }

    @OnClick(R.id.filter)
    public void onClickFilter() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_in_bottom);
        filterFragment.show(transaction, null);
    }

    @OnClick(R.id.imageButton_menu)
    public void openDrawer() {
        mDrawerLayout.openDrawer(Gravity.LEFT);
    }

    @OnClick(R.id.button_reload)
    public void reload() {
        initLoadArticleData();
    }

    public void applyFilters(Set<String> selectedCategories) {
        this.selectedCategories = selectedCategories;
        if (selectedSort == null) {
            selectedSort = Sort.DEFAULT;
        }
        applySorting(selectedSort);
    }

    public void clearFilters() {
        this.selectedCategories = new HashSet<>();
        if (selectedSort == null) {
            selectedSort = Sort.DEFAULT;
        }
        applySorting(selectedSort);
    }

    public void applySorting(Sort sort) {
        selectedSort = sort;
        List<ArticleData> articles = activeArticleData;
        List<ArticleData> sortedList = new ArrayList<>();
        switch (sort) {
            case OPEN_DATE:
                break;
            case NOT_COMPLETED:
                sortedList = getArticlesNotCompleted(articles);
                break;
            case NEVER_OPENED:
                sortedList = getArticlesNeverOpened(articles);
                break;
            case COMPLETED:
                sortedList = getArticlesCompleted(articles);
                break;
            default:
                sortedList = getDefaultList();
        }
        mAdapter.setData(sortedList);
    }

    private List<ArticleData> getDefaultList() {
        List<ArticleData> sortedData;
        if (selectedCategories != null && !selectedCategories.isEmpty()) {
            sortedData = getFilteredList(activeArticleData);
        } else {
            sortedData = activeArticleData;
        }
        return sortedData;
    }

    private List<ArticleData> getArticlesCompleted(List<ArticleData> articles) {
        List<ArticleData> sortedData = new ArrayList<>();
        for (ArticleData articleData : articles) {
            if (articleData.getProgress() == 100) {
                sortedData.add(articleData);
            }
        }
        if (selectedCategories != null && !selectedCategories.isEmpty()) {
            sortedData = getFilteredList(sortedData);
        }
        return sortedData;
    }

    private List<ArticleData> getArticlesNeverOpened(List<ArticleData> articles) {
        List<ArticleData> sortedData = new ArrayList<>();
        for (ArticleData articleData : articles) {
            if (articleData.getProgress() == 0) {
                sortedData.add(articleData);
            }
        }
        if (selectedCategories != null && !selectedCategories.isEmpty()) {
            sortedData = getFilteredList(sortedData);
        }
        return sortedData;
    }

    private List<ArticleData> getArticlesNotCompleted(List<ArticleData> articles) {
        List<ArticleData> sortedData = new ArrayList<>();
        for (ArticleData articleData : articles) {
            if (articleData.getProgress() > 0 && articleData.getProgress() < 100) {
                sortedData.add(articleData);
            }
        }
        if (selectedCategories != null && !selectedCategories.isEmpty()) {
            sortedData = getFilteredList(sortedData);
        }
        return sortedData;
    }

    private List<ArticleData> getFilteredList(List<ArticleData> articles) {
        List<ArticleData> filteredList = new ArrayList<>();
        for (ArticleData articleData : articles) {
            if (articleData.getArticle_category().contains(",")) {
                String[] split = articleData.getArticle_category().split(",");
                for (String aSplit : split) {
                    if (selectedCategories.contains(aSplit)) {
                        filteredList.add(articleData);
                        break;
                    }
                }
            } else if (selectedCategories.contains(articleData.getArticle_category())) {
                filteredList.add(articleData);
            }
        }
        return filteredList;
    }
}
