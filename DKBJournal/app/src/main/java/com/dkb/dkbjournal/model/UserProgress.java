package com.dkb.dkbjournal.model;

/**
 * Created by cbishn on 11/25/17.
 */

public class UserProgress {
    private String id;
    private String user_id;
    private String article_id;
    private int is_complete;
    private float progress_info;
    private String last_open;
    private String created_at;
    private String updated_at;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getArticle_id() {
        return article_id;
    }

    public void setArticle_id(String article_id) {
        this.article_id = article_id;
    }

    public int getIs_complete() {
        return is_complete;
    }

    public void setIs_complete(int is_complete) {
        this.is_complete = is_complete;
    }

    public float getProgress_info() {
        return progress_info;
    }

    public void setProgress_info(float progress_info) {
        this.progress_info = progress_info;
    }

    public String getLast_open() {
        return last_open;
    }

    public void setLast_open(String last_open) {
        this.last_open = last_open;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
