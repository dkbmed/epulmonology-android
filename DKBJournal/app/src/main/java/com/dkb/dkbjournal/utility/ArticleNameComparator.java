package com.dkb.dkbjournal.utility;

import com.dkb.dkbjournal.model.ArticleData;

import java.util.Comparator;

public class ArticleNameComparator implements Comparator<ArticleData> {

    public int compare(ArticleData left, ArticleData right) {
//            return left.getTitle().compareTo(right.getTitle());
//            return Integer.parseInt(left.getId()) - Integer.parseInt(right.getId());
        return Integer.parseInt(right.getId()) - Integer.parseInt(left.getId());
    }
}
