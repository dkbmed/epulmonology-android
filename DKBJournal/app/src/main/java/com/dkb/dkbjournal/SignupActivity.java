package com.dkb.dkbjournal;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bigkoo.alertview.AlertView;
import com.bigkoo.svprogresshud.SVProgressHUD;
import com.dkb.dkbjournal.model.LoginResponse;
import com.dkb.dkbjournal.model.ProfessionData;
import com.dkb.dkbjournal.model.SpecialtyData;
import com.dkb.dkbjournal.utility.DBHelper;
import com.dkb.dkbjournal.utility.Global;
import com.google.gson.Gson;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Call;

public class SignupActivity extends AppCompatActivity {

    /*1. Can you make the home page title ePulmonology Review
2. When I open article, the activity indicator doesnt go away in both the article.
3. The images in the article are not loading. Can you please check
4. The HTML pages that are opening in the webview.. Can you put a activity indicator ?
5. Can you please put the attached Logo*/
    public SVProgressHUD mSVProgressHUD;
    EditText edit_email, edit_first_name, edit_last_name;
    TextView txt_profession, txt_specialty;
    RelativeLayout relativeLayout_list_profession;
    RelativeLayout relativeLayout_list_specialty;
    RelativeLayout relativeLayout_alert;
    TextView text_alert;
    private String strProfessionId;
    private String strSpecialtyId;
    private Boolean is_profession, is_specialty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        strProfessionId = "";
        strSpecialtyId = "";

        is_profession = is_specialty = false;

        text_alert = (TextView) findViewById(R.id.textView_alert_content);
        relativeLayout_alert = (RelativeLayout) findViewById(R.id.relativeLayout_alert);
        relativeLayout_alert.setVisibility(View.INVISIBLE);

        Button btn_alert_ok = (Button) findViewById(R.id.button_alert_ok);
        btn_alert_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                relativeLayout_alert.setVisibility(View.INVISIBLE);
            }
        });

        edit_email = (EditText) findViewById(R.id.editText_email);
        edit_first_name = (EditText) findViewById(R.id.editText_first_name);
        edit_last_name = (EditText) findViewById(R.id.editText_last_name);

        txt_profession = (TextView) findViewById(R.id.textView_profession);
        txt_specialty = (TextView) findViewById(R.id.textView_specialty);

        ImageButton btn_back = (ImageButton) findViewById(R.id.imageButton_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        relativeLayout_list_profession = (RelativeLayout) findViewById(R.id.relativeLayout_list_profession);
        relativeLayout_list_profession.setVisibility(View.GONE);
        relativeLayout_list_profession.setAlpha(0.0f);

        relativeLayout_list_specialty = (RelativeLayout) findViewById(R.id.relativeLayout_list_specialty);
        relativeLayout_list_specialty.setVisibility(View.GONE);
        relativeLayout_list_specialty.setAlpha(0.0f);

        Button btn_register = (Button) findViewById(R.id.button_signup);
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                removeDropMenuView(relativeLayout_list_profession);
                removeDropMenuView(relativeLayout_list_specialty);

                if (edit_email.getText().toString().equals("")) {
                    new AlertView(getString(R.string.alert_title),
                            getString(R.string.alert_enter_email), null,
                            new String[]{getString(R.string.alert_ok)}, null, SignupActivity.this,
                            AlertView.Style.Alert, null).show();
                    return;
                } else if (!Global.isValidEmail(edit_email.getText().toString())) {
                    new AlertView(getString(R.string.alert_title),
                            getString(R.string.alert_enter_email_correct), null,
                            new String[]{getString(R.string.alert_ok)}, null, SignupActivity.this,
                            AlertView.Style.Alert, null).show();
                    return;
                } else if (edit_first_name.getText().toString().equals("")) {
                    new AlertView(getString(R.string.alert_title),
                            getString(R.string.alert_enter_first_name), null,
                            new String[]{getString(R.string.alert_ok)}, null, SignupActivity.this,
                            AlertView.Style.Alert, null).show();
                    return;
                } else if (edit_last_name.getText().toString().equals("")) {
                    new AlertView(getString(R.string.alert_title),
                            getString(R.string.alert_enter_last_name), null,
                            new String[]{getString(R.string.alert_ok)}, null, SignupActivity.this,
                            AlertView.Style.Alert, null).show();
                    return;
                } else if (strProfessionId.equals("")) {
                    new AlertView(getString(R.string.alert_title),
                            getString(R.string.alert_select_profession), null,
                            new String[]{getString(R.string.alert_ok)}, null, SignupActivity.this,
                            AlertView.Style.Alert, null).show();
                    return;
                } else if (strSpecialtyId.equals("")) {
                    new AlertView(getString(R.string.alert_title),
                            getString(R.string.alert_select_specialty), null,
                            new String[]{getString(R.string.alert_ok)}, null, SignupActivity.this,
                            AlertView.Style.Alert, null).show();
                    return;
                } else {

                    mSVProgressHUD = new SVProgressHUD(SignupActivity.this);
                    mSVProgressHUD.showWithStatus(getString(R.string.please_wait), SVProgressHUD.SVProgressHUDMaskType.Clear);

                    OkHttpUtils.get().url(Global.SERVER_SIGNUP)
                            .addParams(Global.KEY_EMAIL, edit_email.getText().toString())
                            .addParams(Global.KEY_FIST_NAME, edit_first_name.getText().toString())
                            .addParams(Global.KEY_LAST_NAME, edit_last_name.getText().toString())
                            .addParams(Global.KEY_PROFESSION_ID, strProfessionId)
                            .addParams(Global.KEY_SPECIALITIES_ID, strSpecialtyId)
                            .build()
                            .execute(new StringCallback() {
                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    Log.d("Respond Error", e.toString());
                                    mSVProgressHUD.dismiss();
                                    text_alert.setText(getString(R.string.alert_signup_failed));
                                    relativeLayout_alert.setVisibility(View.VISIBLE);
                                }

                                @Override
                                public void onResponse(String response, int id) {
                                    Log.d("Respond", response);
                                    LoginResponse loginResponse = new Gson().fromJson(response, LoginResponse.class);

                                    mSVProgressHUD.dismiss();

                                    if (loginResponse.getStatus().equals("2")) {
                                        text_alert.setText(getString(R.string.alert_signup_exist));
                                        relativeLayout_alert.setVisibility(View.VISIBLE);
                                    } else if (loginResponse.getStatus().equals("1")) {
                                        SharedPreferences s = getSharedPreferences("user", 0);
                                        SharedPreferences.Editor edit = s.edit();
                                        edit.putString("email", edit_email.getText().toString());
                                        edit.putString("status", "1");
                                        edit.commit();
                                        DBHelper.saveUserId(loginResponse.getUserId());
                                        Intent intent = new Intent(SignupActivity.this, MainActivity.class);
                                        startActivity(intent);
                                        finish();
                                    } else {
                                        text_alert.setText(getString(R.string.alert_signup_failed));
                                        relativeLayout_alert.setVisibility(View.VISIBLE);
                                    }
                                }
                            });
                }
            }
        });

        RelativeLayout relativeLayout_content = (RelativeLayout) findViewById(R.id.relativeLayout_content);
        RelativeLayout relativeLayout_profession = (RelativeLayout) findViewById(R.id.relativeLayout_profession);
        RelativeLayout relativeLayout_specialty = (RelativeLayout) findViewById(R.id.relativeLayout_specialty);

        relativeLayout_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeDropMenuView(relativeLayout_list_profession);
                removeDropMenuView(relativeLayout_list_specialty);
            }
        });

        relativeLayout_profession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeDropMenuView(relativeLayout_list_specialty);
                showDropMenuView(relativeLayout_list_profession);

                if (!is_profession) {
                    onLoadProfessionData();
                }
            }
        });

        relativeLayout_specialty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeDropMenuView(relativeLayout_list_profession);
                showDropMenuView(relativeLayout_list_specialty);

                if (!is_specialty) {
                    onLoadSpecialtyData();
                }
            }
        });

        onLoadProfessionData();
        onLoadSpecialtyData();
    }

    private void showDropMenuView(final RelativeLayout view) {

        if (view.getVisibility() == View.VISIBLE) {
            removeDropMenuView(view);
        } else {
            view.animate()
                    .alpha(1.0f)
                    .setDuration(150)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            view.setVisibility(View.VISIBLE);
                        }
                    });
        }
    }

    private void removeDropMenuView(final RelativeLayout view) {

        view.animate()
                .alpha(0.0f)
                .setDuration(150)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        view.setVisibility(View.GONE);
                    }
                });
    }

    private void onLoadProfessionData() {

        LinearLayout list = (LinearLayout) findViewById(R.id.linearLayout_list_profession);
        list.removeAllViews();

        OkHttpUtils.get().url(Global.SERVER_PROFESSION)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        Log.d("Respond Error", e.toString());
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        Log.d("Respond", response);
                        try {
                            JSONArray itemArray = new JSONArray(response);
                            if (itemArray != null) {

                                is_profession = true;

                                for (int i = 0; i < itemArray.length(); i++) {
                                    JSONObject e = itemArray.getJSONObject(i);
                                    ProfessionData info = new ProfessionData();

                                    info.setId(Global.onCheckNull(e, Global.KEY_ID));
                                    info.setTitle(Global.onCheckNull(e, Global.KEY_TITLE));
                                    info.setDesc(Global.onCheckNull(e, Global.KEY_DESCRIPTION));

                                    onShowProfessionData(i, info);
                                }
                            }
                        } catch (JSONException e) {
                            Log.d("Json Error", e.toString());
                        }
                    }
                });
    }

    public void onShowProfessionData(int number, final ProfessionData info) {

        LinearLayout list = (LinearLayout) findViewById(R.id.linearLayout_list_profession);

        final LinearLayout newCell = (LinearLayout) (View.inflate(this, R.layout.row_list, null));
        TextView txt_title = (TextView) newCell.findViewById(R.id.textView_list);
        txt_title.setText(info.getTitle());

        newCell.setTag(number);
        registerForContextMenu(newCell);
        list.addView(newCell);

        newCell.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                txt_profession.setText(info.getTitle());
                strProfessionId = info.getId();
                removeDropMenuView(relativeLayout_list_profession);
            }
        });
    }

    private void onLoadSpecialtyData() {

        LinearLayout list = (LinearLayout) findViewById(R.id.linearLayout_list_specialty);
        list.removeAllViews();

        OkHttpUtils.get().url(Global.SERVER_SPECIALTY)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        Log.d("Respond Error", e.toString());
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        Log.d("Respond", response);
                        try {
                            JSONArray itemArray = new JSONArray(response);
                            if (itemArray != null) {

                                is_specialty = true;

                                for (int i = 0; i < itemArray.length(); i++) {
                                    JSONObject e = itemArray.getJSONObject(i);
                                    SpecialtyData info = new SpecialtyData();

                                    info.setId(Global.onCheckNull(e, Global.KEY_ID));
                                    info.setTitle(Global.onCheckNull(e, Global.KEY_TITLE));

                                    onShowSpecialtyData(i, info);
                                }
                            }
                        } catch (JSONException e) {
                            Log.d("Json Error", e.toString());
                        }
                    }
                });
    }

    public void onShowSpecialtyData(int number, final SpecialtyData info) {

        LinearLayout list = (LinearLayout) findViewById(R.id.linearLayout_list_specialty);

        final LinearLayout newCell = (LinearLayout) (View.inflate(this, R.layout.row_list, null));
        TextView txt_title = (TextView) newCell.findViewById(R.id.textView_list);
        txt_title.setText(info.getTitle());

        newCell.setTag(number);
        registerForContextMenu(newCell);
        list.addView(newCell);

        newCell.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                txt_specialty.setText(info.getTitle());
                strSpecialtyId = info.getId();
                removeDropMenuView(relativeLayout_list_specialty);
            }
        });
    }
}
