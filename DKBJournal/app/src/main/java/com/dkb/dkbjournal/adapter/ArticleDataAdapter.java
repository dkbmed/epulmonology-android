package com.dkb.dkbjournal.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bigkoo.alertview.AlertView;
import com.bigkoo.svprogresshud.SVProgressHUD;
import com.dkb.dkbjournal.DetailHtmlViewActivity;
import com.dkb.dkbjournal.PdfActivity;
import com.dkb.dkbjournal.R;
import com.dkb.dkbjournal.model.ArticleData;
import com.dkb.dkbjournal.network.DkbRetrofitBuilder;
import com.dkb.dkbjournal.utility.FileUtility;
import com.dkb.dkbjournal.utility.Global;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Callback;
import timber.log.Timber;


public class ArticleDataAdapter extends BaseAdapter {

    public SVProgressHUD mSVProgressHUD;
    String pdfName;
    String htmlName;
    private Context mContext;
    private List<ArticleData> mArticleDataList = new ArrayList<>();

    public ArticleDataAdapter(Context context) {
        mContext = context;
        mSVProgressHUD = new SVProgressHUD(mContext);
    }

    public void insertItem(ArticleData item) {
        mArticleDataList.add(0, item);
        notifyDataSetChanged();
    }

    public void addItem(ArticleData item) {
        mArticleDataList.add(item);
        notifyDataSetChanged();
    }

    public void removeAll() {
        mArticleDataList = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void replaceItem(int pos, ArticleData item) {
        mArticleDataList.set(pos, item);
        notifyDataSetChanged();
    }

    public void removeItem(int pos) {
        mArticleDataList.remove(pos);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mArticleDataList != null ? mArticleDataList.size() : 0;
    }

    @Override
    public ArticleData getItem(int position) {
        return mArticleDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.row_journal, parent, false);
            convertView.setTag(viewHolder = new ViewHolder(convertView));
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final ArticleData articleData = mArticleDataList.get(position);

        viewHolder.txt_title.setText(articleData.getTitle());
        viewHolder.txt_content.setText(articleData.getDescription());
        viewHolder.progressIndicator.setMax(100);
        viewHolder.progressIndicator.setProgress(articleData.getProgress());

        Log.e("Thumb ", " : " + articleData.getThumb());
        if (articleData.getThumb().equals("")) {
            viewHolder.image_Article.setImageResource(R.mipmap.ic_launcher);
        } else {
            UrlImageViewHelper.setUrlDrawable(viewHolder.image_Article, "http://epulm.eliteraturereview.org/public/article_thumbs/" + articleData.getThumb(), R.mipmap.ic_launcher);
        }

        viewHolder.relativeLayout_article.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (articleData.getArticle_path().contains(".pdf") || articleData.getArticle_path().contains(".PDF")) {

                    String[] items = articleData.getArticle_path().split("\\/");
                    pdfName = items[items.length - 1];

                    File file = new File(Environment.getExternalStorageDirectory() + "/DBKJournal/", pdfName);
                    if (file.exists()) {
                        onDisplayPDF(articleData);
                    } else if (!Global.isNetworkAvailable(mContext)) {
                        new AlertView(mContext.getString(R.string.alert_title),
                                mContext.getString(R.string.alert_need_internet), null,
                                new String[]{mContext.getString(R.string.alert_ok)}, null, mContext,
                                AlertView.Style.Alert, null).show();
                    } else {
                        downloadFile(articleData, pdfName);
                      /*  OkHttpUtils.get(articleData.getArticle_path())
                                .tag(this)
                                .execute(new DownloadFileCallBack(articleData, Environment.getExternalStorageDirectory() + "/DBKJournal", pdfName));*/
                    }

                } else if (articleData.getArticle_path().contains(".html") || articleData.getArticle_path().contains(".htm")) {

                    if (!Global.isNetworkAvailable(mContext)) {

                        String[] items = articleData.getArticle_path().split("\\/");
                        htmlName = articleData.getId() + "_" + items[items.length - 1];

                        File file = new File(Environment.getExternalStorageDirectory() + "/DBKJournal/", htmlName);
                        if (file.exists()) {
                            Global.is_online = "offline";
                            Intent intent = DetailHtmlViewActivity.getIntent(mContext, articleData);
                            mContext.startActivity(intent);
                        } else {
                            new AlertView(mContext.getString(R.string.alert_title),
                                    mContext.getString(R.string.alert_need_internet), null,
                                    new String[]{mContext.getString(R.string.alert_ok)}, null, mContext,
                                    AlertView.Style.Alert, null).show();
                        }

                    } else {
                        Global.is_online = "online";
                        Intent intent = DetailHtmlViewActivity.getIntent(mContext, articleData);
                        mContext.startActivity(intent);
                    }

                } else {
                    Intent intent = DetailHtmlViewActivity.getIntent(mContext, articleData);
                    mContext.startActivity(intent);
                }
            }
        });

        return convertView;
    }

    private void downloadFile(final ArticleData articleData, final String pdfName) {
        mSVProgressHUD.showWithStatus(mContext.getString(R.string.please_wait), SVProgressHUD.SVProgressHUDMaskType.Clear);

        new DkbRetrofitBuilder().getService().downloadFile(articleData.getArticle_path()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    FileUtility.writeResponseBodyToDisk(response.body(), pdfName);
                    mSVProgressHUD.dismiss();
                    onDisplayPDF(articleData);
                } else {
                    Timber.d("server contact failed");
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {
                Timber.e("error");
                mSVProgressHUD.dismiss();
            }
        });
    }

    private void onDisplayPDF(ArticleData articleData) {
        mContext.startActivity(PdfActivity.getIntent(mContext, articleData));
    }

    public List<ArticleData> getData() {
        return mArticleDataList;
    }

    public void setData(List<ArticleData> articleData) {
        mArticleDataList = articleData;
        notifyDataSetChanged();
    }

    public class ViewHolder {

        public ImageView image_Article;
        public TextView txt_title, txt_content;
        public RelativeLayout relativeLayout_article;
        public ProgressBar progressIndicator;

        ViewHolder(View rootView) {
            initView(rootView);
        }

        private void initView(View rootView) {

            txt_title = (TextView) rootView.findViewById(R.id.textView_title);
            txt_content = (TextView) rootView.findViewById(R.id.textView_content);
            image_Article = (ImageView) rootView.findViewById(R.id.imageView_journal);
            relativeLayout_article = (RelativeLayout) rootView.findViewById(R.id.relativeLayout_article);
            progressIndicator = (ProgressBar) rootView.findViewById(R.id.progress_indicator);
        }
    }
}
