package com.dkb.dkbjournal.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by cbishn on 11/25/17.
 */

public class Category {
    @SerializedName("ID")
    private String id;
    @SerializedName("Category")
    private String category;
    @SerializedName("Disease")
    private String disease;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDisease() {
        return disease;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }
}
