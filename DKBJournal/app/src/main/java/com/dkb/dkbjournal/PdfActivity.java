package com.dkb.dkbjournal;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bigkoo.svprogresshud.SVProgressHUD;
import com.dkb.dkbjournal.model.ArticleData;
import com.dkb.dkbjournal.model.ArticleResponse;
import com.dkb.dkbjournal.network.DkbRetrofitBuilder;
import com.dkb.dkbjournal.utility.DBHelper;
import com.joanzapata.pdfview.PDFView;

import java.io.File;
import java.util.List;

import retrofit2.Callback;
import retrofit2.Response;

public class PdfActivity extends AppCompatActivity {
    public static final String ARTICLE_DATA = "ARTICLE_DATA";
    private PDFView pdfView;
    private String pdfName;
    private ArticleData articleData;
    private SVProgressHUD mSVProgressHUD;
    private int index;

    public static Intent getIntent(Context context, ArticleData articleData) {
        Intent intent = new Intent(context, PdfActivity.class);
        intent.putExtra(ARTICLE_DATA, articleData);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf);

        pdfView = (PDFView) findViewById(R.id.pdfview);
        TextView txt_title = (TextView) findViewById(R.id.textView_title);
        ImageButton btn_back = (ImageButton) findViewById(R.id.imageButton_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        articleData = getIntent().getParcelableExtra(ARTICLE_DATA);

        txt_title.setText(articleData.getTitle());

        String[] items = articleData.getArticle_path().split("\\/");
        pdfName = items[items.length - 1];
        index = DBHelper.getIndex(articleData.getId());
        File file = new File(Environment.getExternalStorageDirectory() + "/DBKJournal", pdfName);
        if (file.exists()) {
            pdfView.fromFile(file)
                    .showMinimap(false)
                    .enableSwipe(true)
                    .load();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (index == -1) {
                        if (articleData.getProgress() > 0) {
                            int i = articleData.getProgress() * pdfView.getPageCount();
                            index = (int) ((double) (i) / (double) 100);
                        } else {
                            index = 0;
                        }
                    }
                    pdfView.jumpTo(index);
                }
            }, 200);
        }
        mSVProgressHUD = new SVProgressHUD(PdfActivity.this);
    }

    private void saveProgress(final int progress) {
        new DkbRetrofitBuilder().getService().saveArticleProgress(articleData.getId(), DBHelper.getUserId(), progress).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(retrofit2.Call<Void> call, Response<Void> response) {
                Log.d("Successful", "success");
                ArticleResponse articleResponse = DBHelper.getArticleData();
                List<ArticleData> articles = articleResponse.getArticles();
                articleData.setProgress(progress);
                int i = articles.indexOf(articleData);
                articles.set(i, articleData);
                articleResponse.setArticles(articles);
                DBHelper.saveArticleDataWithoutProgress(articleResponse);
                finish();
            }

            @Override
            public void onFailure(retrofit2.Call<Void> call, Throwable t) {
                Log.d("Failure", "failure");
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        int readPages = pdfView.getCurrentPage() + 1;
        DBHelper.saveIndex(articleData.getId(), readPages);
        int progress = (readPages * 100) / pdfView.getPageCount();
        saveProgress(progress);
    }
}
