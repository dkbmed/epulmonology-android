package com.dkb.dkbjournal.model;

/**
 * Created by cbishn on 11/26/17.
 */

public enum Sort {
    OPEN_DATE,
    NEVER_OPENED,
    COMPLETED,
    NOT_COMPLETED,
    DEFAULT
}
